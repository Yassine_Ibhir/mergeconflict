/* Yassine Ibhir
 * 1612502
 * Section 1
 */
package lab02;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle [] bicycles = new Bicycle[4];
		
		bicycles[0] = new Bicycle("DECATHLON",10,60);
		bicycles[1] = new Bicycle("CCM",15,50);
		bicycles[2] = new Bicycle("RALI",18,45.8);
		bicycles[3] = new Bicycle("BIXI",3,30);
		
		for(Bicycle b: bicycles) {
			System.out.println(b);
		}
	}

}
