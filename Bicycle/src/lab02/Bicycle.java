/* Yassine Ibhir
 * 1612502
 * Section 1
 */
package lab02;

public class Bicycle {

	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer,int numberGears,double maxSpeed) {
		
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
		
	}

	//Getters
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getnumberGears() {
		return numberGears;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
//override toString()
	public String toString() {
		return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears +", Maximum Speed: " + maxSpeed;
	}
	
}
