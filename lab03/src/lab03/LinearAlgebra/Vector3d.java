// Yassine Ibhir
// 1612502
package lab03.LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x,double y,double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static void main(String [] args) {
		Vector3d vec1=new Vector3d(4,4,2);
		Vector3d vec2= new Vector3d(2,2,2);
		System.out.println(vec1.magnitude());
		System.out.println(vec2.magnitude());
		System.out.println(vec1.dotProduct(vec2));
		Vector3d sum= vec1.add(vec2);
		System.out.println(sum.x+" "+sum.y+" "+sum.z);
	}
	
	// getters
	// @return double vector points
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	//calculating magnitude
	// @return double magnitude
	
	public double magnitude() {
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	// calculating dotProduct of two vectors
	//@param Vector3d 
	//@return double dorProduct result
	
	public double dotProduct(Vector3d vec) {
		return this.x*vec.getX()+this.y*vec.getY()+this.z*vec.getZ();
	}
	//calculating the sum of two vectors
	//@param Vector3d 
	//@return Vector3d
	
	public Vector3d add(Vector3d vec) {
		double x = this.x+vec.getX();
		double y = this.y+vec.getY();
		double z = this.z+vec.getZ();
		return  new Vector3d(x,y,z);
		}
}
