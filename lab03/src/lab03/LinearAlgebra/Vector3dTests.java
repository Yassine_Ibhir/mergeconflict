// Yassine Ibhir
// 1612502
package lab03.LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	public void testVector3d() {
		
		Vector3d vec = new Vector3d(2,-4,2.33);
		assertEquals(2,vec.getX());
		assertEquals(-4,vec.getY());
		assertEquals(2.33,vec.getZ());
	}
	
	@Test
	public void testMagnitude() {
		
		Vector3d vec=new Vector3d(6,3,-3);
		assertEquals(7.34,vec.magnitude(),0.009);
		
		Vector3d vec1=new Vector3d(4,3,0);
		assertEquals(5.0,vec1.magnitude());
		
	}
	
	@Test
	public void testDotProduct() {
		
		Vector3d vec1=new Vector3d(4,4,-2.4);
		Vector3d vec2=new Vector3d(2,2,2);
		
		assertEquals(11.2,vec1.dotProduct(vec2));
		
	}
	
	@Test
	public void testSum() {
		
		Vector3d vec1=new Vector3d(-4,4.5,-7);
		Vector3d vec2=new Vector3d(2,-2,2);
		Vector3d expected = new Vector3d(-2,2.5,-5);
		Vector3d actual = vec1.add(vec2);
		
		
		assertEquals(expected.getY(),actual.getY());
		assertEquals(expected.getY(),actual.getY());
		assertEquals(expected.getZ(),actual.getZ());
		
	}


}
